-------------------------------------------------------------
-- Set of functions for creating selector pictures formspec --
--------------------------------------------------------------

-- Next functions intends to create a formspec letting you choose one 
-- or several pictures from textures folder, given a pattern prefix
local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

level_editor.get_pic_list = function(path,prefix)
	local pic_list = minetest.get_dir_list(path,false)
	local messages = {}
	local patterndef
	if prefix then
		patterndef = prefix..".+%.png" --or "^info_.+%.png"
	else patterndef = ".+%.png"
	end
	print("pattern "..patterndef)
	print("pictures dir : "..path)
	print("pic list : "..dump(pic_list))
	for i,v in ipairs(pic_list) do
		if level_editor.check_file(path..v) and v:match(patterndef) then
			local _,message,_= v:match("^("..prefix..")(.*)(%.png)$")
			--print(" message : "..message)
			table.insert(messages,message)
		end
	end
	print("pic list treatement : "..dump(messages))
	return messages
end


local mk_formspec = nil
mk_formspec = function(path,prefix,messages)
	local fwidth = 15
	local fheight = 11
	local message_formspec = "size["..fwidth..","..fheight..",false]"..
		"no_prepend[]"
	local p,c = math.modf(math.sqrt(fheight))	
	for i,v in ipairs(messages) do
		local e,r = math.modf((i-1)/p)
		local xpos = e*p
	--	if i <= fheight-7 then
		local ypos = i-xpos
		ypos = ypos*2+(ypos-1) 				
		local length = #messages
		local f,s = math.modf((length-1)/p)
		xpos = xpos+(fwidth/2-(2*f-1)) 
		message_formspec = message_formspec.."image_button["..xpos..","..ypos..";2,2;"..prefix..v..".png;"..v..";]"
	end
	return message_formspec
end

function level_editor.make_formspec(folder,prefix)
	local path = modpath.."/"..folder.."/"
	--minetest.mkdir(path)	
	local messages = {}
	messages = level_editor.get_pic_list(path,prefix)
	local formspec = mk_formspec(path,prefix,messages)
	print("dump formspec "..dump(formspec))
	return formspec
end



	-- Next functions register allpictures of a folder as wallmounted nodes 
	-- with name formed like: level_editor:basenodename_picturename
function level_editor.register_image_as_node(nodename,prefix,picturename)
	assert(type(nodename)  ==  "string")
	local completename
	local picname
	if prefix and picturename then
		picname = prefix..picturename..".png"
		completename = nodename.."_"..picturename
	else
		picname = nodename..".png"
		completename = nodename
	end
	local node = modname..":"..completename
	--print("newnode name : "..node)
	--print("completename : "..completename)
	minetest.register_node(node, {
		description = completename,
		drawtype = "signlike",
		tiles = {picname},
		visual_scale = 1.0,
		inventory_image = picname,
		wield_image = picname,
		paramtype = "light",
		paramtype2 = "wallmounted",
		sunlighmt_propagates = true,
		walkable = false,
		selection_box = {
			type = "wallmounted"
		},
		groups = {oddly_breakable_by_hand=3, picture=1, not_in_craft_guide=1, not_in_creative_inventory=1},
	})
	print("new node registered : "..dump(node))
	return node
end
			

local level_editor_can_dig = nil
local priv_for_dig = "level"
level_editor_can_dig = function(pos,player)
	local plname = player:get_player_name()
	local keys=player:get_player_control()
	local playerpriv = minetest.get_player_privs(plname)
	if playerpriv[priv_for_dig] == true and keys["aux1"] == true then
		return true
	end
end



function level_editor.register_images_as_nodes(folder,nodename,prefix,priv)
	local nodes = {}
	local node = level_editor.register_image_as_node(nodename)
	minetest.override_item(node,{groups = {oddly_breakable_by_hand=3, 
		picture=1, not_in_craft_guide=1} 
	})
	table.insert(nodes,node)
	
	local node_pic_path = modpath.."/".."textures".."/"
	local path = modpath.."/"..folder
	local messages = level_editor.get_pic_list(node_pic_path,prefix)
	for i,message in ipairs(messages) do
		local node = level_editor.register_image_as_node(nodename, prefix, message)
		table.insert(nodes,node)
	end
	for i,node in ipairs(nodes) do
		if priv then
			priv_for_dig = priv
			minetest.override_item(node,{can_dig = level_editor_can_dig})
			minetest.override_item(node,
				{on_construct = function(pos) 
					--print("enter in on_construct rewrite ")
					local meta = minetest.get_meta(pos)
					local formspec = level_editor.make_formspec("textures", prefix)
					formspec = formspec..
						"label[2,0;Click on picture infos]"..
						"button_exit[13,0;2,1;ok;OK]"..
						"button_exit[13,1;2,1;clear;Clear]"
					meta:set_string("formspec",formspec)
				end
			})
		end
	end
end

