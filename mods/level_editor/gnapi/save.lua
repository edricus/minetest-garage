
local storage = minetest.get_mod_storage()
local __player_data
local __level_data
local __protection_data
local __checkpoint_data

-- Table Save Load Functions
function level_editor.save()
	print("save called")
	storage:set_string("player_data",minetest.serialize(__player_data))
	storage:set_string("level_data",minetest.serialize(__level_data))
	storage:set_string("protection_data",minetest.serialize(__protection_data))
	storage:set_string("checkpoint_data",minetest.serialize(__checkpoint_data))

end

function level_editor.load()
	print("load called")
	__player_data = minetest.deserialize(storage:get_string("player_data")) or {}
	__level_data = minetest.deserialize(storage:get_string("level_data")) or {}
	__protection_data = minetest.deserialize(storage:get_string("protection_data")) or {}
	__checkpoint_data = minetest.deserialize(storage:get_string("checkpoint_data")) or {}
end

function level_editor.getleveldata(level)
	assert(type(level) == "string")
	local data = __level_data[level] or {}

	data.base_def = data.base_def or {}
	data.count_gen = data.count_gen or 0
	data.protected = data.protected or {}

	__level_data[level] = data
	return data
end
	
function level_editor.getprotectiondata()
	local pdata = __protection_data or {}
	__protection_data = pdata
	--level_editor.save()
	return pdata
end

function level_editor.getcheckpointdata(checkpoint_pos)
	assert(type(checkpoint_pos) == "string")
	local data = __checkpoint_data[checkpoint_pos] or {}
	data.infos = data.infos or {}
	data.pos = minetest.string_to_pos(checkpoint_pos)
	__checkpoint_data[checkpoint_pos] = data
	--level_editor.save()
	return data
end

function level_editor.getplayerdata(name)
	assert(type(name) == "string")
	local data = __player_data[name] or {}
	
	data.name     = data.name or name
	data.checkpoint = data.checkpoint or {}
	data.actual_level = data.actual_level or ""
	data.registered_levels = data.registered_levels or {}
	data.hud_level_infos = data.hud_level_infos or {}
	__player_data[name] = data
	return data
end


function level_editor.player_or_nil(name)
	return __player_data[name]
end

function level_editor.enable(name)
	level_editor.player(name).disabled = nil
end

function level_editor.disable(name)
	level_editor.player(name).disabled = true
end

function level_editor.clear_player(name)
	__player_data[name] = {}
end
