function level_editor.protect_area(startpos,endpos,area_name,owner)
	owner = owner or ""
	local prot = level_editor.getprotectiondata()
	local area = {}
	area.area_name = area_name
	area.owner = owner
	area.startpos = startpos
	area.endpos = endpos
	prot[area_name] = area
	print(" new area protected : "..dump(prot[area_name]))
	print("pos in protectiondata : "..table.getn(prot))
	level_editor.save()
end

function level_editor.check_protection(pos,name)
	local prot = level_editor.getprotectiondata()
	for area,areadef in pairs(prot) do
		local v = {}
		v = table.copy(areadef)

		print("digger : "..name.." ; owner : "..v.owner.." for "..v.area_name.." between "..v.startpos.x.." - "..v.endpos.x)
		
		if	name ~= v.owner and
		(pos.x >= v.startpos.x and pos.x <= v.endpos.x) and
		(pos.y >= v.startpos.y and pos.y <= v.endpos.y) and
		(pos.z >= v.startpos.z and pos.z <= v.endpos.z) then
			print("digged in previous shownzone at "..minetest.pos_to_string(pos))
			return true
		end
	end
end
	
local old_is_protected = minetest.is_protected
function minetest.is_protected(pos, name)
	if level_editor.check_protection(pos,name) == true then
		minetest.chat_send_player(name,"can't dig here")
		return true
	end
	return old_is_protected(pos, name)
end
