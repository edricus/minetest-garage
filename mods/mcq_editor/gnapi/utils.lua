	-- Check if a file is readable
mcq_editor.check_file = function(filepath)
--local filename	= minetest.get_modpath("mcq_editor").."/textures/info_"..infoname..".png"
	local file		= io.open(filepath, "r")
	if file ~= nil then
		io.close(file)
		return filepath
	else return nil
	end
end	

	--  Round a pos to a 1-decimal value (for pos)
mcq_editor.round = {}
mcq_editor.round = function(table)
	local round_pos = {}
	for k,v in pairs(table) do
		if v >=0 then
			round_pos[k]= math.floor(v*10 + 0.5)/10
		else 
			round_pos[k]= math.ceil(v*10 - 0.5)/10
		end
	end
	return round_pos
end

