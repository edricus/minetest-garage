local modname = minetest.get_current_modname()

local good_answer_permanent_callback = nil
good_answer_permanent_callback = function(plname)
	print("good answer")
end

local bad_answer_permanent_callback = nil
bad_answer_permanent_callback = function(plname)
	print("bad answer")
end

local on_lock_callback = nil
on_lock_callback = function(plname,question)
	print(question.." locked")
end

local on_unlock_question = nil
on_unlock_question = function(plname,question)
	minetest.log("action", plname.." has unlocked question "..question)
	local qudef = mcq_editor.mcq(question)
	--local qudef = mcq_editor.questions[question]
	
	-- Give Prizes
	if qudef and qudef.prizes then
		for i = 1, #qudef.prizes do
			local itemstack = ItemStack(qudef.prizes[i])
			if not itemstack:is_empty() then
				local receiverref = minetest.get_player_by_name(plname)
				if receiverref then
					receiverref:get_inventory():add_item("main", itemstack)
				end
			end
		end
	end
end

local on_too_late_answer_callback = nil
on_too_late_answer_callback = function(plname,question)
	print("good answer but "..question.." already locked")
end

minetest.register_on_player_receive_fields(function(player, formname, fields)
-- Called when formname is one of defined questions
	local fname,question = string.match(formname,"^("..modname..":pic_)(.+)")
	if fname and question then
		local plname = player:get_player_name()
		local qudef = mcq_editor.mcq(question)
		
		local data = mcq_editor.player(plname)
		data.attempts[question] = data.attempts[question] or 0
		for i,_ in pairs(fields) do
			--[[ Test fields
	We will now test the fields received after user clicked on a button 
	inside a form , or press Escape, etc.
	Fields are table, we test indexes:
	it can match with reponse (aka good answer, defined with question)
	or with bad_answer, or just escape the form without callback
	--]]
			if i ~= qudef.reponse and i ~= "quit" then
				bad_answer_permanent_callback(plname)
			elseif i == qudef.reponse then
				good_answer_permanent_callback(plname)
			end
			if not qudef.limited or qudef.limited == 0 then
				if i ~= qudef.reponse and i ~= "quit" and not data.unlocked[question] then
					data.attempts[question] = data.attempts[question] + 1
				elseif i == qudef.reponse and not data.unlocked[question] then	
					data.unlocked[question] = question
					on_unlock_question(plname,question)
				end
			elseif qudef.limited and qudef.limited > 0 then
				if i ~= qudef.reponse and i ~= "quit" and not data.unlocked[question] then
					data.attempts[question] = data.attempts[question] + 1
					if data.attempts[question] == qudef.limited then
						data.locks[question] = question
						on_lock_callback(plname,question)
					end
				elseif i == qudef.reponse and not data.unlocked[question] then
					if data.locks[question] == question then
						on_too_late_answer_callback(plname,question)
					else 
						data.unlocked[question] = question
						on_unlock_question(plname,question)
					end
				end
			end
		end
		mcq_editor.save()
	end
end)
