local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

-- load questions templates from .conf files
mcq_editor.register_question = function(question)
	local mcq = mcq_editor.mcq(question)
	
	local questions_dir = modpath.."/questions/"
	local textures_dir = modpath.."/textures/"
	local settings = Settings(questions_dir..question..".conf")
	local conf = settings:to_table()

	for k,v in pairs(conf) do
		mcq[k] = v
	end

	mcq.name = mcq.name or question
	
	local pics = {"pic","form"}
	for i,v in ipairs(pics) do
		local default = v.."_"..mcq.name..".png"

		if mcq_editor.check_file(textures_dir..default) ~= nil then
			mcq[v] = conf[v] or default
		else
			mcq[v] = conf[v] or "default-image.png"
		end
	end
	if mcq.limited then mcq.limited = tonumber(mcq.limited) end

	mcq_editor.save()
end

mcq_editor.register_questions = function()
	local questions_dir = modpath.."/questions/"
	local base_questions = minetest.get_dir_list(questions_dir,false)  -- false returns only file names
	for i,v in ipairs(base_questions) do
		if v:match("(%.conf)$") then
			local question,_ = v:match("(.-)(%.conf)$")
			mcq_editor.register_question(question)
		end
	end
end

